/**
    Description: A first attempt at implementing a fraction class.

    Author:      Ricardo Salazar 
    Date:        October 3, 2016. 
 */


#include <iostream>

// using namespace std; // <-- Bad practice. Avoid!
using std::cout;


// A Fraction has a NUMERATOR and a DENOMINATOR. 
// We sould also add CONSTRUCTORS, ARITHMETIC OPERATIONS
// and maybe a function that allows us to OUTPUT the 
// contents to the console.
class Fraction{
   private:
      int numerator;
      int denominator;

   public:

      // No param. constr. Produces fraction 0/1
      Fraction();  

      // One param. constr. Produces fraction n/1
      Fraction( int n );  

      // Two param. constr. Produces value n/d
      // ASSUMES d is not zero.
      Fraction( int n, int d );  

      // Operations (Wish-list)
      // +, -, *, /,   
      // +=, =-, =*, =/, ++ and -- for now.
      // 

      // Let us start with a fake one...
      Fraction plus_equals( const Fraction& f );

      // Then continue with a member operator
      Fraction operator+( const Fraction& rhs ) const;

      // Displays fraction to console
      void print() const;  // <-- Why const?
};



// Implementation of member functions
Fraction::Fraction(){
   numerator = 0;
   denominator = 1;
}

Fraction::Fraction( int n ){
   numerator = n;
   denominator = 1;
}

Fraction::Fraction( int n, int d ){
   numerator = n;
   denominator = d;
}


// Some other member functions
Fraction Fraction::plus_equals( const Fraction& f ){
   /**
       We write more code here in an effort to make 
       our implementation cleaner and easier to 
       follow.
 
       We use the formula
           a / b + c / d = (a*d + b*c) / (b*d); 
   */ 
   
   int a = numerator;
   int b = denominator;
   int c = f.numerator;
   int d = f.denominator;

   // return Fraction( a*d + b*c , b*d ); // <-- What's wrong here?

   numerator = a*d + b*c;
   denominator = b*d;

   return *this;
}

Fraction Fraction::operator+( const Fraction& rhs ) const {
   // COPY/PASTING is the best... or is it???
   int a = numerator;
   int b = denominator;
   int c = rhs.numerator;
   int d = rhs.denominator;

   return Fraction( a*d + b*c , b*d );
}


void Fraction::print() const {
   cout << numerator << "/" << denominator;
   return;
}

// Next: Simple arithmetic operators and plus_equals  

int main(){ 

   cout << "\nNow lets turn our attention to Fractions...\n"; 
   Fraction f1;        // defaults to 0/1 
   Fraction f2(2);     // defaults to 2/1 
   Fraction f3(1, 2);  // create 1/2 
   
   cout << "Fraction f1;    Fraction f2(2);    "; 
   cout << "Fraction f3(1, 2);\nproduces the values:    "; 

   cout << "f1 = "; 
   f1.print(); cout << "    "; 
   cout << "f2 = "; 
   f2.print(); cout << "    "; 
   cout << "f3 = "; 
   f3.print(); cout << ".\n"; 


   // Continue testing other functions
   cout << "\nNext, let us call the plus_equals function:\n"; 
   cout << "f2.plus_equals(f3) returns the fraction "; 
   f2.plus_equals(f3).print(); 
   cout << ", which is the right answer.\n"; 
   cout << "Also, f2 does store the correct value, namely: "; 
   
   // Here we display the value using << instead of print() 
   cout << " f2 = "; 
   f2.print(); 
   cout << "\n\n"; 
   
   cout << "However, notice that although we can 'chain' plus_equals,"; 
   cout << "\nit doesn't behave like += applied to int's. "; 
   cout << "The call:\n"; 
   cout << "f2.plus_equals(f3).plus_equals( Fraction(3,2) )\n"; 
   cout << "should first update f3, then add this new value to f2.\n"; 
   cout << "\nIn other words, after the call we should have:\n"; 
   cout << "f3 = 8/4,    f2 = 36/8."; 
   f2.plus_equals(f3).plus_equals( Fraction(3,2) ); 
   
   cout << "\nInstead we have:\n"; 
   cout << "\nf3 = ";
   f3.print(); 
   cout << ",    f2 = ";
   f2.print(); 
   cout  << ".\n"; 
   

   cout << "\nLastly notice that addition (+) is kinda broken:\n"; 
   cout << "Fraction(2,3) + 1 works, it returns " ; 

   Fraction f4 = Fraction(2,3) + 1 ;
   f4.print();
   cout << ". However,\n"; 
   cout << "1 + Fraction(2,3) does not work!!!\n"; 
   
   // Fraction f5 = 1 + Fraction(2,3) ;
   // ^^^ uncomment to produce a compiler error

   return 0;
}
