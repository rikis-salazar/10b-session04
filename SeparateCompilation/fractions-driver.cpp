/**
   Final tester for our Fraction class

   Author:	Ricardo Salazar
   Date:	October 2016
 */

#include <iostream>   // std::cout
#include "fractions.h"

using std::cout;

int main(){

   cout << "Hello, 'Fractioned'-am...ed-finalized world!\n\n";

   cout << "Final tests ...\n"; 
   Fraction f1;         // 0/1 
   Fraction f2(2);      // 2/1 
   Fraction f3(3, -6);  // (-1)/2 
   
   cout << "Fraction f1;    Fraction f2(2);    "; 
   cout << "Fraction f3(3, -6);\nProduces:\t"; 
   cout << "f1 = " << f1 << "\t"; 
   cout << "f2 = " << f2 << "\t"; 
   cout << "f3 = " << f3 << "\n"; 

   cout << "\nExpression\t\tResult\t\tOperator\n"
        << "  -f3\t\t\t" << -f3 << "\t\tunary (-)\n"
	<< "  2 - f3\t\t" <<  2 - f3 << "\t\tbinary (-)\n"
	<< "  -( f3 - 2 )\t\t" << -( f3 - 2 ) << "\t\tunary & binary (-)\n";

   f1 = f1 + f3;
   cout	<< "  f1 = f1 + f3\t\tf1 = " << f1 << "\t(+)\n"
        << "  f2 * f3\t\t" << f2 * f3 << "\t\t(*)\n";

   Fraction f4 = f1 / f2;
   cout << "  f4 = f1 / f2\t\tf4 = " << f4 << "\t(/)\n\n";

   f4 += 3;
   cout << "  f4 += 3\t\tf4 = " << f4 << "\t(+=)\n";

   f4 -= f2;
   cout << "  f4 -= f2\t\tf4 = " << f4 << "\t(-=)\n";

   f2 *= f4;
   cout << "  f2 *= f4\t\tf2 = " << f2 << "\t(*=)\n";

   f1 /= f2;
   cout << "  f1 /= f2\t\tf1 = " << f1 << "\t(/=)\n";

   cout << "\nf1 holds the value " << f1 << ".\n";
   cout << "The statement  ' cout << f1++ ; '  displays the value "
        << f1++ << ",\n";
   cout << "and afterwards f1 holds the value " << f1 << ".\n";

   cout << "\nOn the other hand, f4 holds the value " << f4 << ".\n";
   cout << "The statement  ' cout << ++f4 ; '  displays the value "
        << ++f4 << ",\n";
   cout << "and afterwards f4 holds the value " << f4 << ".\n";

   return 0;
}
