/** 
   Implementation of a Fraction class.

   Author: 	Ricardo Salazar
   Date:	October 2016
*/

#ifndef __FRACTIONS_H__
#define __FRACTIONS_H__

#include <iostream>   // std::ostream, std::istream

using std::ostream;
using std::istream;

class Fraction{
   private:

      /** The numerator of the class */
      int numerator;

      /** The denominator of the class */
      int denominator;



      // -------- AUXILIARY PRIVATE FUNCTIONS --------

      /** 
          Comparison between Fraction objects. 

	  param: NONE
	  return: an int value that is 
	           - postive if calling fraction is 
		     bigger than the parameter.
		   - negative if calling fraction is
		     smaller than the parameter.
		   - zero if both fractions are equal. 
      */
      int compare( const Fraction& f ) const;

      /** 
          Computes the greatest common divisor (gcd)
	  of the numerator and denominator fields.
	  
	  param: NONE 
	  return: the gcd of the private fields.
      */
      int gcd() const;

      /** 
          Reduces Fractions. E.g. 2/(-4) --> (-1)/2
	  
	  param: NONE 
	  return: VOID
      */
      void normalize();


   public:
      /**
         Constructor

	 param:- NONE, or
	        - the numerator of the fraction, or
		- the numerator and the denominator
		  of the fraction.
      */
      Fraction( int num = 0 , int denom = 1 );   



      // -------- GETTERS AND SETTERS SECTION --------  

      /**
         Accesor function num()
        
         param: NONE
	 return: a const reference to the value stored in numerator
      */
      const int& num() const;

      /**
         Accesor function denom()
        
         param: NONE
	 return: a const reference to the value stored in denominator
      */
      const int& denom() const;

      /**
         Mutator function num() 
        
         param: NONE 
	 return: a reference to 'numerator'
      */
      int& num();

      /**
         Mutator function denom()
        
	 param: NONE
	 return: a reference to 'denominator'
      */
      int& denom();



      // -------- ARITHMETIC OPERATORS ( MEMBERS ) --------- 

      /** 
         operator+= as in  ( f += g )

	 param: A Fraction object to be added to implicit object
	 return: the implicit object (by reference).
      */
      Fraction& operator+=( const Fraction& rhs );

      /** 
         operator*= as in  ( f *= g )

	 param: A Fraction object to be multiplied times implicit object
	 return: the implicit object (by reference).
      */
      Fraction& operator-=( const Fraction& rhs );
 
      /** 
         operator-= as in  ( f -= g )

	 param: A Fraction object to be subtracted from implicit object
	 return: the implicit object (by reference).
      */
      Fraction& operator*=( const Fraction& rhs );
 
      /** 
         operator/= as in  ( f /= g )

	 param: The Fraction object g in the expression f/g
	 return: the implicit object (by reference).
      */
      Fraction& operator/=( const Fraction& rhs );
 


      // ---- INCREMENT/DECREMENT OPERATORS ( MEMBERS ) ----  

      /** 
         Prefix increment operator ++f

	 param: NONE
	 return: a reference to the fraction being increased
       */
      Fraction& operator++();

      /** 
         Prefix decrement operator --f

	 param: NONE
	 return: a reference to the fraction being decreased
       */
      Fraction& operator--();

      /** 
         Postfix increment operator f++

	 param: an UNUSED int. To differentiate it from prefix version 
	 return: the fraction being increased
       */
      Fraction operator++( int unused );

      /** 
         Postfix decrement operator f--

	 param: an UNUSED int. To differentiate it from prefix version 
	 return: the fraction being decreased
       */
      Fraction operator--( int unused );



   // ------- FRIENDS SECTION  (BOOLEAN OPER.) -------  

   /**
      Nom member operator< 
      
      param: the Fraction objects (lhs,rhs) in lhs < rhs 
      return: true if lhs < rhs, false otherwise.  
   */ 
   friend bool operator<( const Fraction& lhs, const Fraction& rhs );

   /**
      Nom member operator> 
      
      param: the Fraction objects in lhs > rhs 
      return: true if lhs > rhs, false otherwise. 
   */
   friend bool operator>( const Fraction& lhs, const Fraction& rhs );

   /**
      Nom member operator<= 
      
      param: the Fraction objects in lhs <= rhs 
      return: true if lhs <= rhs, false otherwise. 
   */
   friend bool operator<=( const Fraction& lhs, const Fraction& rhs );

   /**
      Nom member operator>= 
      
      param: the Fraction objects in lhs >= rhs 
      return: true if lhs >= rhs, false otherwise. 
   */
   friend bool operator>=( const Fraction& lhs, const Fraction& rhs );

   /**
      Nom member operator== 
      
      param: the Fraction objects in lhs == rhs 
      return: true if lhs == rhs, false otherwise. 
   */
   friend bool operator==( const Fraction& lhs, const Fraction& rhs );

   /**
      Nom member operator!=
  
      param: the Fraction objects in lhs != rhs
      return: true if lhs != rhs, false otherwise. 
   */
   friend bool operator!=( const Fraction& lhs, const Fraction& rhs );

};



/**********************************************************
  
     N O N   M E M B E R   F U N C T I O N S
  
 **********************************************************/

// -------------- OUTPUT to std::ostream --------------  

/**
   operator<< as in cout << Fraction(1,2);
   
   param: out, std::ostream object where the Fraction is sent
          rhs, Fraction object to be sent to stream
   return: a reference to the first object passed as paremeter
 */
ostream& operator<<( ostream& out, const Fraction& rhs );



// -------------- INPUT to std::istream ---------------  

/**
   operator>> as in Fraction f; cin >> f;
   
   param: _______FILL_IN_THE_BLANK_HERE_______
   return: _______FILL_IN_THE_BLANK_HERE_______

   note: To make your life easier you might want
         to make it a friend of the class.
 */
istream& operator>>( istream& in, Fraction& rhs );



// ----------- UNARY ARITHMETIC OPERATIONS -----------  

/**
   Change of sign operator (UNARY MINUS)
  
   param: the Fraction object f in the expression -f
   return: the negative of the fraction passed as param.
 */
Fraction operator-( const Fraction& f ); 



// ---------- BINARY ARITHMETIC OPERATIONS -----------  

/**
   Addition of fractions
  
   param: the Fraction objects (lhs,rhs) in lhs + rhs
   return: the Fraction object lhs + rhs
 */
Fraction operator+( Fraction lhs, const Fraction& rhs ); 

/**
   Subtraction of fractions
  
   param: the Fraction objects (lhs,rhs) in lhs - rhs
   return: the Fraction object lhs - rhs
 */
Fraction operator-( Fraction lhs, const Fraction& rhs ); 

/**
   Multiplication of fractions
  
   param: the Fraction objects (lhs,rhs) in lhs * rhs
   return: the Fraction object lhs * rhs
 */
Fraction operator*( Fraction lhs, const Fraction& rhs ); 

/**
   Division of fractions
  
   param: the Fraction objects (lhs,rhs) in lhs / rhs
   return: the Fraction object lhs / rhs
 */
Fraction operator/( Fraction lhs, const Fraction& rhs ); 


#endif
