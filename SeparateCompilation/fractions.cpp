/**
   Implementation of member functions of the 
   Fraction class.

   Author:	Ricardo Salazar
   Date:	October 2016
*/

#include <iostream>   // std::ostream, std::istream
#include "fractions.h" 

using std::ostream; 
using std::istream; 



// Constructor(s) 
Fraction::Fraction( int num, int denom ) 
   : numerator(num), denominator(denom) { normalize(); }



// Auxiliary functions ( private members ) 
int Fraction::compare( const Fraction& f ) const {
   // The relation between a/b & c/d is determined by a*d - b*c 
   int a  = numerator;
   int b  = denominator;
   int c  = f.numerator;
   int d  = f.denominator;

   return a*d - b*c ;
}

int Fraction::gcd() const{
   int n = numerator;
   int m = denominator;

   // Procedure based on Euclid's Greatest 
   // Common Divisor algorithm. See: 
   // http://en.wikipedia.org/wiki/Euclidean_algorithm
   // Apply only if both numbers are strictly positive
   while (n != m) {   
      if (n < m)  
         m = m - n;
      else 
         n = n - m;
   }   
   
   return n;
}

void Fraction::normalize(){
   // Recall the ternary operator 
   //     var = cond ? val1 : val2
   // is the same as
   //    if ( cond ) var = val1; else var = val2;
   int sign = ( numerator * denominator  >=0 ) ? 1 : -1 ;

   // Extract values (ignore signs)
   numerator = ( numerator >= 0 ) ? numerator : -numerator ;
   denominator = ( denominator >= 0 ) ? denominator : -denominator ;

   // If numerator is not 0 find the gcd and reduce
   int c = 1;
   if ( numerator > 0 )
      c = gcd();

   // Keep sign in numerator and reduce
   numerator = sign * ( numerator / c ) ; 
   denominator /= c;

   return;
}



// Getters and Setters ...
const int& Fraction::num() const {
   return numerator;
}

const int& Fraction::denom() const {
   return denominator;
}

int& Fraction::num(){
   return numerator;
}

int& Fraction::denom(){
   return denominator;
}



// MEMBER OPERATORS
Fraction& Fraction::operator+=( const Fraction& rhs ){
   // a/b + c/d = ( a*d + b*c )/( b*d )
   int a = this->numerator;
   int b = this->denominator;
   int c = rhs.numerator;
   int d = rhs.denominator;

   this->numerator = a*d + b*c ;
   this->denominator = b*d ;

   normalize(); 
   return *this;
}

Fraction& Fraction::operator-=( const Fraction& rhs ){
   // f - g = f + (-g) 
   return *this += -rhs ;  
}

Fraction& Fraction::operator*=( const Fraction& rhs ){
   // a/b * c/d = ( a*c )/( b*d )
   int a = this->numerator;
   int b = this->denominator;
   int c = rhs.numerator;
   int d = rhs.denominator;

   this->numerator = a*c ;
   this->denominator = b*d ;

   normalize(); 
   return *this;
}

Fraction& Fraction::operator/=( const Fraction& rhs ){
   // (a/b) / (c/d) = ( a*d )/( b*c )
   int a = this->numerator;
   int b = this->denominator;
   int c = rhs.numerator;
   int d = rhs.denominator;

   this->numerator = a*d ;
   this->denominator = b*c ;

   normalize(); 
   return *this;
}



// INCREMENT/DECREMENT OPERATORS
Fraction& Fraction::operator++(){
   return *this += 1;
}

Fraction& Fraction::operator--(){
   return *this -= 1;
}

Fraction Fraction::operator++( int unused ){
   Fraction clone(*this);  
   //       ^^^^^ Copy constructor. 
   //       The code is provided by the compiler.
   //       We'll talk about this later.

   ++(*this);
   return clone;
}

Fraction Fraction::operator--( int unused ){
   Fraction clone = *this;
   //       ^^^^^ Also calls the Copy constructor. 

   --(*this);
   return clone;
}



// --------------------------------------------------- 
// ------------------- NON MEMBERS ------------------- 
// --------------------------------------------------- 

// STREAM OPERATORS
ostream& operator<<( ostream& out, const Fraction& rhs ){
   out << rhs.num(); 
   if ( rhs.denom() != 1 )
      out << "/" << rhs.denom();

   return out;
}

istream& operator>>( istream& in, Fraction& rhs ){
   //
   // YOUR TURN, HAVE FUN!!!
   //
   // #FoundOne #ExamQuestion #EasterEgg #OneMoreHashTag
   //
   return in;
}


// UNARY ARITHMETIC OPERATORS
Fraction operator-( const Fraction& f ){
   return Fraction( -f.num(), f.denom() );
}



// BINARY ARITHMETIC OPERATORS (Note: lhs passed by value)
Fraction operator+( Fraction lhs, const Fraction& rhs ){
   return lhs += rhs ;
}

Fraction operator-( Fraction lhs, const Fraction& rhs ){
   return lhs -= rhs ;
}

Fraction operator*( Fraction lhs, const Fraction& rhs ){
   return lhs *= rhs ;
}

Fraction operator/( Fraction lhs, const Fraction& rhs ){
   return lhs /= rhs ;
}



// BOOLEAN OPERATORS (friends)
bool operator<( const Fraction& lhs, const Fraction& rhs ){
   return lhs.compare(rhs) < 0 ;
}

bool operator>( const Fraction& lhs, const Fraction& rhs ){
   return lhs.compare(rhs) > 0 ;
}

bool operator<=( const Fraction& lhs, const Fraction& rhs ){
   return lhs.compare(rhs) <= 0 ;
}

bool operator>=( const Fraction& lhs, const Fraction& rhs ){
   return lhs.compare(rhs) >= 0 ;
}

bool operator==( const Fraction& lhs, const Fraction& rhs ){
   return lhs.compare(rhs) == 0 ;
}

bool operator!=( const Fraction& lhs, const Fraction& rhs ){
   return !( lhs == rhs );
}

